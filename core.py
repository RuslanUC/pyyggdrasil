from base64 import b64decode, b64encode

from asyncpg import create_pool
from typing import Optional, Union
from hashlib import md5
from utils import escape_string, genToken
from time import time

class User:
    def __init__(self, uuid: str, login: str, email: str, password: str, _core):
        self.uuid = uuid
        self.login = login
        self.email = email
        self.password = password
        self._core = _core

    def verify(self, password: str) -> bool:
        m = md5()
        m.update(password.encode("utf8"))
        return m.hexdigest() == self.password

    async def accessToken(self, clientToken: str) -> str:
        accessToken = genToken()
        await self._core.addSession(self, clientToken, accessToken)
        return accessToken

class PartialUser:
    def __init__(self, uuid: str, _core):
        self.uuid = uuid
        self._core = _core

    accessToken = User.accessToken

class Session:
    def __init__(self, uuid: str, clientToken: str, accessToken: str, expires: int, _core):
        self.uuid = uuid
        self.clientToken = clientToken
        self.accessToken = accessToken
        self.expires = expires
        self._core = _core

    async def delete(self) -> None:
        await self._core.delSession(self)

    async def refresh(self) -> str:
        await self.delete()
        return await PartialUser(self.uuid, self._core).accessToken(self.clientToken)

class Core:
    def __init__(self):
        self._pool = None

    async def initDB(self, conn_string) -> None:
        self._pool = await create_pool(conn_string, min_size=5, max_size=20)

    async def getUser(self, email: str) -> Optional[User]:
        async with self._pool.acquire() as db:
            if user := await db.fetch(f"SELECT * FROM users WHERE email='{escape_string(email)}' LIMIT 1;"):
                user = user[0]
                return User(user["uuid"], user["login"], user["email"], user["password"], self)

    async def getUserByUUID(self, uuid: str) -> Optional[User]:
        async with self._pool.acquire() as db:
            if user := await db.fetch(f"SELECT * FROM users WHERE uuid='{uuid}' LIMIT 1;"):
                user = user[0]
                return User(user["uuid"], user["login"], user["email"], user["password"], self)

    async def getUserByLogin(self, login: str) -> Optional[User]:
        async with self._pool.acquire() as db:
            if user := await db.fetch(f"SELECT * FROM users WHERE login='{escape_string(login)}' LIMIT 1;"):
                user = user[0]
                return User(user["uuid"], user["login"], user["email"], user["password"], self)

    async def addSession(self, user: User, clientToken: str, accessToken: str) -> None:
        async with self._pool.acquire() as db:
            await db.execute(f"INSERT INTO sessions (uuid, client_token, access_token, expires)"
                             f"VALUES ('{user.uuid}', '{clientToken}', '{accessToken}', '{int(time()+604800)}');")

    async def getSession(self, accessToken: str, allowExpired=False) -> Optional[Session]:
        async with self._pool.acquire() as db:
            if sess := await db.fetch(f"SELECT * FROM sessions WHERE access_token='{escape_string(accessToken)}'"):
                sess = sess[0]
                sess = Session(sess["uuid"], sess["client_token"], sess["access_token"], int(sess["expires"]), self)
                if not allowExpired and time() > sess.expires:
                    await sess.delete()
                    return
                return sess

    async def delSession(self, session: Session) -> None:
        async with self._pool.acquire() as db:
            await db.execute(f"DELETE FROM sessions WHERE uuid='{session.uuid}' AND client_token='{session.clientToken}'"
                             f"AND access_token='{session.accessToken}';")

    async def addJoinRequest(self, uuid: str, server: str, ip: str) -> None:
        async with self._pool.acquire() as db:
            await db.execute(f"INSERT INTO join_sessions (uuid, server_id, user_ip) VALUES ('{uuid}', '{server}', '{ip}');")

    async def getJoinRequest(self, uuid: str, server: str) -> Optional[dict]:
        async with self._pool.acquire() as db:
            if sess := await db.fetch(f"SELECT * FROM join_sessions WHERE uuid='{uuid}' AND server_id='{server}'"):
                sess = sess[0]
                return dict(sess)

    async def delJoinRequest(self, uuid: str, server: str) -> None:
        async with self._pool.acquire() as db:
            await db.execute(f"DELETE FROM join_sessions WHERE uuid='{uuid}' AND server_id='{server}';")

    async def getSkin(self, user: Union[User, PartialUser]) -> Optional[bytes]:
        async with self._pool.acquire() as db:
            if skin := await db.fetch(f"SELECT skin FROM skins WHERE uuid='{escape_string(user.uuid)}';"):
                skin = skin[0]
                return b64decode(skin["skin"].encode("utf8"))

    async def uploadSkin(self, uuid: str, skin: bytes) -> None:
        skin = b64encode(skin).decode("utf8")
        async with self._pool.acquire() as db:
            if await db.fetch(f"SELECT skin FROM skins WHERE uuid='{uuid}';"):
                await db.execute(f"DELETE FROM skins WHERE uuid='{uuid}';")
            await db.execute(f"INSERT INTO skins (uuid, skin) VALUES ('{uuid}', '{skin}');")

    async def delSkin(self, uuid: str) -> None:
        async with self._pool.acquire() as db:
            await db.execute(f"DELETE FROM skins WHERE uuid='{uuid}';")