from base64 import b64encode
from datetime import datetime
from json import dumps
from os import environ
from time import time

from quart import Quart, request

from core import Core, PartialUser
from utils import genToken, c_json, uuidFromStr

app = Quart("yggdrasil")
core = Core()

@app.before_serving
async def before_serving():
    await core.initDB(conn_string=environ.get("DB"))

@app.route("/auth/authenticate", methods=["POST"])
async def auth_authenticate():
    data = await request.get_json()
    if not data.get("username") or not data.get("password"):
        return c_json({
            "errorType": "FORBIDDEN",
            "error": "ForbiddenOperationException",
            "errorMessage": "Forbidden",
            "developerMessage": "Forbidden"
        }, 401)
    if not (user := await core.getUser(data.get("username").lower())) or not user.verify(data.get("password")):
        return c_json({
            "errorType": "FORBIDDEN",
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid credentials. Invalid username or password.",
            "developerMessage": "Invalid credentials. Invalid username or password."
        }, 403)
    clientToken = data.get("clientToken") or genToken()
    accessToken = await user.accessToken(clientToken)
    profile = {"name": user.login, "id": user.uuid}
    resp = {
        "clientToken": clientToken,
        "accessToken": accessToken,
        "availableProfiles": [profile],
        "selectedProfile": profile
    }
    if data.get("requestUser"):
        resp["user"] = {
            "id": int.from_bytes(bytes.fromhex(user.uuid.split("-")[0]), 'big'),
            "username": user.email,
            "properties": [{
                "name": "preferredLanguage",
                "value": "en-US"
            },
                {
                    "name": "registrationCountry",
                    "value": "UA"
                }
            ]
        }
    return c_json(resp)

@app.route("/auth/refresh", methods=["POST"])
async def auth_refresh():
    data = await request.get_json()
    if not data.get("clientToken") or not data.get("accessToken"):
        return c_json({
            "errorType": "FORBIDDEN",
            "error": "ForbiddenOperationException",
            "errorMessage": "Forbidden",
            "developerMessage": "Forbidden"
        }, 401)
    if not (sess := await core.getSession(data.get("accessToken"), True)):
        return c_json({
            "errorType": "FORBIDDEN",
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid token.",
            "developerMessage": "Invalid token."
        }, 403)
    accessToken = await sess.refresh()
    user = await core.getUserByUUID(sess.uuid)
    resp = {
        "clientToken": sess.clientToken,
        "accessToken": accessToken,
        "selectedProfile": {"name": user.login, "id": user.uuid}
    }
    if data.get("requestUser"):
        resp["user"] = {
            "id": int.from_bytes(bytes.fromhex(user.uuid.split("-")[0]), 'big'),
            "username": user.email,
            "properties": [
                {
                "name": "preferredLanguage",
                "value": "en-US"
                },
                {
                    "name": "registrationCountry",
                    "value": "UA"
                }
            ]
        }
    return c_json(resp)

@app.route("/account/profiles/minecraft", methods=["POST"])
async def account_profiles_minecraft():
    data = await request.get_json()
    if not isinstance(data, list) or not data:
        return c_json({
            "errorType": "BAD_REQUEST",
            "error": "Bad Request",
            "errorMessage": "No body supplied.",
            "developerMessage": "No body supplied."
        }, 400)
    if len(data) > 5:
        data = data[:5]
    users = []
    for u in data:
        if (user := await core.getUserByLogin(u)):
            users.append({"id": user.uuid, "name": user.login})
    return c_json(users)

@app.route("/session/session/minecraft/join", methods=["POST"])
async def session_session_minecraft_join():
    data = await request.get_json()
    if not data.get("accessToken") or not data.get("selectedProfile") or not data.get("serverId"):
        return c_json({
            "errorType": "BAD_REQUEST",
            "error": "Bad Request",
            "errorMessage": "One or more required fields was missing.",
            "developerMessage": "One or more required fields was missing."
        }, 400)
    uuid = uuidFromStr(data.get("selectedProfile"))
    if not (sess := await core.getSession(data.get("accessToken"))) or sess.uuid != uuid:
        return c_json({
            "errorType": "FORBIDDEN",
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid token.",
            "developerMessage": "Invalid token."
        }, 403)
    ip = request.headers.get('cf-connecting-ip') or request.remote_addr
    await core.addJoinRequest(uuid, data.get("serverId"), ip)
    return ""

@app.route("/session/session/minecraft/hasJoined", methods=["GET"])
async def session_session_minecraft_hasjoined():
    data = request.args
    if not data.get("username") or not data.get("serverId"):
        print("idk")
        return c_json({
            "errorType": "BAD_REQUEST",
            "error": "Bad Request",
            "errorMessage": "One or more required fields was missing.",
            "developerMessage": "One or more required fields was missing."
        }, 400)
    if not (user := await core.getUserByLogin(data.get("username"))):
        return c_json({
            "errorType": "BAD_REQUEST",
            "error": "Bad Request",
            "errorMessage": "Profile does not exist.",
            "developerMessage": "Profile does not exist."
        }, 400)
    if not (sess := await core.getJoinRequest(user.uuid, data.get("serverId"))):
        return c_json({
            "errorType": "FORBIDDEN",
            "error": "ForbiddenOperationException",
            "errorMessage": "Player not joined.",
            "developerMessage": "Player not joined."
        }, 401)
    elif data.get("ip") and sess["user_ip"] != data.get("ip"):
        return c_json({
            "errorType": "FORBIDDEN",
            "error": "ForbiddenOperationException",
            "errorMessage": "IP address does not match.",
            "developerMessage": "IP address does not match."
        }, 401)

    await core.delJoinRequest(user.uuid, data.get("serverId"))
    textures = {}
    if await core.getSkin(user):
        textures["SKIN"] = {"url": f"https://{request.host}/textures/skins/{user.uuid}"}
    j = {
        "timestamp": int(time()*1000),
        "profileId": user.uuid,
        "profileName": user.login,
        "textures": textures
    }
    j = b64encode(dumps(j).encode("utf8")).decode("utf8")
    return c_json({
        "id": user.uuid,
        "name": user.login,
        "properties": [{"name": "textures", "value": j}]
    })

@app.route("/services/player/attributes", methods=["GET"])
async def services_player_attributes():
    return c_json({
        "privileges": {
            "onlineChat": {
                "enabled": True
            },
            "multiplayerServer": {
                "enabled": True
            },
            "multiplayerRealms": {
                "enabled": False
            },
            "telemetry": {
                "enabled": False
            }
        },
        "profanityFilterPreferences": {
            "profanityFilterOn": False
        }
    })

@app.route("/services/privacy/blocklist", methods=["GET"])
async def services_privacy_blocklist():
    return c_json({
        "blockedProfiles": []
    })

@app.route("/services/player/certificates", methods=["POST"])
async def services_player_certificates():
    #accessToken = request.headers.get("Authorization", "").replace("Bearer ", "")
    #if not (sess := await core.getSession(accessToken)):
    #    return c_json({
    #        "errorType": "FORBIDDEN",
    #        "error": "ForbiddenOperationException",
    #        "errorMessage": "Invalid token.",
    #        "developerMessage": "Invalid token."
    #    }, 403)
    #privKey = RSA.generate(2048)
    #pubKey = privKey.publickey()
    #pubKeyE = pubKey.exportKey("DER")
    #t = int(time())
    #dt = datetime.utcfromtimestamp(t)
    #expires = (dt+timedelta(hours=48)).strftime("%Y-%m-%dT%H:%M:%S.000000Z")
    #refresh = (dt+timedelta(hours=36)).strftime("%Y-%m-%dT%H:%M:%S.000000Z")

    ## publicKeySignature
    #publicKeySignature = pkcs1_15.new(privKey).sign(SHA1.new(f'{t+48*3600}000{pubKeyE}'.encode("utf8")))
    #publicKeySignature = b64encode(publicKeySignature).decode("utf8")

    ## publicKeySignatureV2
    #data = b''
    #u = UUID(sess.uuid)
    #msb, lsb = unpack(">qq", u.bytes)
    #data += msb.to_bytes(8, "big")
    #data += lsb.to_bytes(8, "big", signed=True)
    #data += ((t+48*3600)*1024).to_bytes(8, "big")
    #data += pubKeyE
    #publicKeySignatureV2 = pss.new(privKey).sign(SHA1.new(data))
    #publicKeySignatureV2 = b64encode(publicKeySignatureV2).decode("utf8")

    #privKeyS = privKey.exportKey(pkcs=8).decode("utf-8").split("\n")
    #pubKeyS = pubKey.exportKey().decode("utf-8").replace("PUBLIC KEY", "RSA PUBLIC KEY").split("\n")

    #rh, rf = privKeyS.pop(0), privKeyS.pop(-1)
    #privKeyS = rh + "\n" + "".join(privKeyS) + "\n" + rf
    #rh, rf = pubKeyS.pop(0), pubKeyS.pop(-1)
    #pubKeyS = rh + "\n" + "".join(pubKeyS) + "\n" + rf

    privKeyS = "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC3L+nx3Mhwxuk1FBj6VEBkkPZPpU5zaihbTUPtLHueH8NgVEUy3H3t0XlM3l+ipWV6jd9H+4TlM3iifnWdnJjiO+xL+0NNqIpQOLwnCLBIeNJKGSjG9AkER/sojY5wpTngWkLFV0yaIrk6k8WcPPEBt9bEzqcPPsJP2f0sA1II08iM1YSIp8AZA03MbEKcLWE/lQMvYs7Isk10CVeZS5mWzdSjZAmsUbMz+g7OUAeQ8pVurzH4pQKnE3saPXhhCA8L3fLvHiyj7ud6xYUQ6dx22nR3bYrmCF6lO63MRaRx2KfD+1CuqloMSFvBrmd2Bux0Xj4sotRpP45+KWDapiHZAgMBAAECggEACjV6X9yXsxPyxTQp3DGRJnRJtdCW7v8DNsC3QLgnlNKyVw5QjkVpeIp3AcisIxVSnXeF8liwZFGaWdFFtBNDwJU/AFEFbtuDXHXSTolBFxjcp1KASkN+s9ALIuHDXBW+hE6jb/oDLofcM3PZdxvnq9qB9pU0YY4e0se+6z1YIgkLU2EuIg36DYlzP9FG+bxagvO/NL2QETUVFXYQddRiHvMeTV0EfpB18M/EmiBMCXBEfDPPmwfGLiLkAnQplKYuKGpdWTYtWRC5sv1TA5YEVu7wngSpVQJYph+b1prgf9Avjsm+aZbZZq5DKM7+hL201jHTQi50N/Zui401WOuYHQKBgQDYR6+DIIwrYwsPf39LS8ShADdG+AeUUIJ731WB4C2AWlZWC+JzoQUjnYeMQGFHn8dDfkyw/1mRiDmseVYsgNlNqc8Er2fCouUNtUbz5sX0wSkVLwM4FZIhvB/v5DkhfiAUf/CpkysQnJosI+HoC1oQkU4TFnxhGRI53IqErAwYqwKBgQDY1GGkMOQJeMRv1j79CZHUvV+vzJNQzFtlRhJxAhI1sZLz9mN61LlJAlbvTFVacZp92pa2ayT+ROHtd+M0jFiFP7knhUD+az8RMAhqLSDxbsNq2dxE0acRxV3dYt7N2wYeMLjFYyLt7cbGtPEcEWKpMaMGwUPuY6P2CArhFFg3iwKBgQCtbLYA+G1NMQsib0LcVrIhw7GPd71kITawK4I+NUtnG4kVUExSGbCsQVn6jXkz3JwEs9a9KtRgjRJqCZWe8+bLjrlQcXJLMSFoX464brkXLHhL4zm6lBlscZROYPuqrp78sSaCY17p4BkEAfXYgpnCpTEsvyAXH+pseTqdbfP5JwKBgQDMWLv8xabj1lErJTsPtikd2auIPsDHcRDhjPxsTItk1wmrqhVu+XP1fqL3u3TlXBNBcPSAztF6YOibqFUUfQAf9EJHIKp0HBa/2AjWJ9nYL48z8EZLBVrywJMJxdg9IaHOahR40klk+qZPNs/Up/3XftQp1ZJz5Kvtfv7hyQ6qqQKBgAssc5a4t8Tr5gI5mCxr7yAn1FHGJFrv1gvJR0g4Wt2qfkMWwpcUdiQCKNh5MWWfPCMQQKVLGPfHc+LNhitnNOh+SbG1xtmExFZRRFgftI6xaCG7eGOEXn4zcoyxqKc0KB8yZNfFPhAJYXWSa6a3JGWzsRwMWVfe5DeSuLPlVpFJ\n-----END PRIVATE KEY-----"
    pubKeyS = "-----BEGIN RSA PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAty/p8dzIcMbpNRQY+lRAZJD2T6VOc2ooW01D7Sx7nh/DYFRFMtx97dF5TN5foqVleo3fR/uE5TN4on51nZyY4jvsS/tDTaiKUDi8JwiwSHjSShkoxvQJBEf7KI2OcKU54FpCxVdMmiK5OpPFnDzxAbfWxM6nDz7CT9n9LANSCNPIjNWEiKfAGQNNzGxCnC1hP5UDL2LOyLJNdAlXmUuZls3Uo2QJrFGzM/oOzlAHkPKVbq8x+KUCpxN7Gj14YQgPC93y7x4so+7nesWFEOncdtp0d22K5ghepTutzEWkcdinw/tQrqpaDEhbwa5ndgbsdF4+LKLUaT+Ofilg2qYh2QIDAQAB\n-----END RSA PUBLIC KEY-----"
    publicKeySignature = publicKeySignatureV2 = "AA=="
    expires = datetime(2030, 1, 1).strftime("%Y-%m-%dT%H:%M:%S.000000Z")
    refresh = datetime(2029, 1, 1).strftime("%Y-%m-%dT%H:%M:%S.000000Z")

    return c_json({
        "keyPair": {
            "privateKey": privKeyS,
            "publicKey": pubKeyS
        },
        "publicKeySignature": publicKeySignature,
        "publicKeySignatureV2": publicKeySignatureV2,
        "expiresAt": expires,
        "refreshedAfter": refresh
    })

@app.route("/services/minecraft/profile/skins", methods=["POST"])
async def services_minecraft_profile_skins():
    accessToken = request.headers.get("Authorization", "").replace("Bearer ", "")
    if not (sess := await core.getSession(accessToken)):
        return c_json({
            "errorType": "FORBIDDEN",
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid token.",
            "developerMessage": "Invalid token."
        }, 403)
    if request.content_length > 1024*1024*8:
        return c_json({
            "errorType": "BAD_REQUEST",
            "error": "Bad Request",
            "errorMessage": "Payload too big.",
            "developerMessage": "Payload too big."
        }, 400)
    files = await request.files
    if "file" not in files:
        return c_json({
            "errorType": "BAD_REQUEST",
            "error": "Bad Request",
            "errorMessage": "Please send a file or URL.",
            "developerMessage": "Please send a file or URL."
        }, 400)
    skin = files["file"].read()
    await core.uploadSkin(sess.uuid, skin)
    return ""

@app.route("/services/minecraft/profile", methods=["GET"])
async def services_minecraft_profile():
    accessToken = request.headers.get("Authorization", "").replace("Bearer ", "")
    if not (sess := await core.getSession(accessToken)):
        return c_json({
            "errorType": "FORBIDDEN",
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid token.",
            "developerMessage": "Invalid token."
        }, 403)
    if not (user := await core.getUserByUUID(sess.uuid)):
        return c_json({
            "errorType": "BAD_REQUEST",
            "error": "Bad Request",
            "errorMessage": "Profile does not exist.",
            "developerMessage": "Profile does not exist."
        }, 400)
    skin = await core.getSkin(user)
    skin = [] if not skin else [{"id": user.uuid, "state": "ACTIVE", "variant": "MAIN", "url": f"https://{request.host}/textures/skins/{user.uuid}"}]
    return c_json({
        "id": user.uuid,
        "name": user.login,
        "skins": skin,
        "capes": []
    })

@app.route("/textures/skins/<string:uuid>", methods=["GET"])
async def textures_skins_uuid(uuid: str):
    skin = await core.getSkin(PartialUser(uuid, core))
    if not skin:
        return "", 404
    return skin, 200, {"Content-Type": "image/png"}

@app.route("/rollout/v1/msamigration", methods=["GET"])
def rollout_v1_msamigration():
    return c_json({
        "feature": "msamigration",
        "rollout": False
    })

@app.route("/")
def index():
    return "ok"

if __name__ == "__main__":
    from uvicorn import run as urun

    urun("main:app", host="0.0.0.0", port=8000, reload=True, use_colors=False)
