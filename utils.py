from random import choice
from string import digits, ascii_letters
from json import dumps

def escape_string(value): # From pymysql
    _escape_table = [chr(x) for x in range(128)]
    _escape_table[0] = "\\0"
    _escape_table[ord("\\")] = "\\\\"
    _escape_table[ord("\n")] = "\\n"
    _escape_table[ord("\r")] = "\\r"
    _escape_table[ord("\032")] = "\\Z"
    _escape_table[ord('"')] = '\\"'
    _escape_table[ord("'")] = "\\'"

    return value.translate(_escape_table)

def genToken():
    return "".join([choice(digits+ascii_letters) for _ in range(128)])

def c_json(resp, code=200):
    if not isinstance(resp, str):
        resp = dumps(resp)
    return resp, code, {"Content-Type": "application/json"}

def uuidFromStr(s: str) -> str:
    return f"{s[0:8]}-{s[8:12]}-{s[12:16]}-{s[16:20]}-{s[20:32]}"